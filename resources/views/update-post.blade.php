@extends('layouts.app')

@section('content')

<div class="container">
        <h3>{{$post->title}}</h3>
        <p>{{$post->description}}</p><br>
        <b>Author: {{$post->user->name}}</b>
</div>

@endsection