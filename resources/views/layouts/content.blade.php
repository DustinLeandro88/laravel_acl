<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
        <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

        <!-- CSS externos -->
        <link rel="stylesheet" type="text/css"  href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css"  href="{{asset('css/datatables.min.css')}}">
        <link rel="stylesheet" type="text/less"  href="{{asset('css/style.less')}}">

        <title>Analitika</title>
    </head>
    @yield('content')


        <!-- JS externos -->
        <script type="text/javascript" src="{!! asset('js/jquery-3.3.1.slim.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/jquery-3.3.1.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/datatables.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/bootstrap.bundle.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/bootstrap.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/less.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/style.js') !!}"></script>
    </body>
</html>