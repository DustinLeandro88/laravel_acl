@extends('auth.templates.template')

@section('content-form')

<form class="login form border border-primary rounded bg-white" method="POST" action="{{ route('login') }}">
    @csrf
    <div class="form-row mx-4">
        <div class="form-group col-2 mr-2">
            <input id="email" type="email" placeholder="E-mail" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-row mx-4">
        <div class="form-group col-2 mr-2">
            <input id="password" type="password" placeholder="Senha" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-row mx-4">
        <div class="form-group col-2 mr-2">
            <button type="submit" class="btn btn-login">
                {{ __('Login') }}
            </button>
            @if (Route::has('password.request'))
                <a class="recuperar" href="{{ route('password.request') }}">
                    {{ __('Recuperar senha?') }}
                </a>
            @endif
        </div>
    </div>
</form>

@endsection
