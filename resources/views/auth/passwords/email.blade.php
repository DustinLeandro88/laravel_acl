@extends('auth.templates.template')

@section('content-form')
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif

<form class="form login bg-white" method="POST" action="{{ route('password.email') }}">
    @csrf
    <div class="form-row mx-4">
        <div class="form-group col-2 mr-2">
            <input id="email" type="email" placeholder="E-mail" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-row mx-4">
        <div class="form-group col-2 mr-2">
            <button type="submit" class="btn btn-login">
                {{ __('Enviar link para recuperar de senha') }}
            </button>
        </div>
    </div>
</form>
@endsection
