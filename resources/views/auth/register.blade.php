@extends('auth.templates.template')

@section('content-form')
<form class="login form border border-primary rounded bg-white" method="POST" action="{{ route('register') }}">
    @csrf
    <div class="form-row mx-4">
        <div class="form-group col-2 mr-2">
            <input id="name" type="text" placeholder="Nome" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-row mx-4">
        <div class="form-group col-2 mr-2">
            <input id="email" type="email" placeholder="E-mail" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-row mx-4">
        <div class="form-group col-2 mr-2">
            <input id="password" type="password" placeholder="Senha" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-row mx-4">
        <div class="form-group col-2 mr-2">
            <input id="password-confirm" type="password" placeholder="Confirmar senha" class="form-control" name="password_confirmation" required autocomplete="new-password">
        </div>
    </div>

    <div class="form-row mx-4">
        <div class="form-group col-2 mr-2">
            <button type="submit" class="btn btn-login">
                {{ __('Register') }}
            </button>
        </div>
    </div>
</form>
@endsection
