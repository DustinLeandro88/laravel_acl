<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Gate;

class HomeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Post $post){
        $posts = $post->all();
        // $posts = $post->where('user_id', auth()->user()->id)->get();

        return view('home', compact('posts'));
    }

    public function update($id){
        $post = Post::find($id);

        // $this->authorize('update-post', $post);
        if(Gate::denies('edit_post', $post)){
            abort(403,'Não autorizado!');
        }

        return view('update-post', compact('post'));
    }

    public function rolesPermissions(){
        $nameUser = auth()->user()->name;
        echo("<h3>{$nameUser}</h3>");

        foreach(auth()->user()->roles as $role){
            echo "<b>$role->name</b> -> ";

            foreach($role->permissions as $permission){
                echo " $permission->name, ";
            }

            echo '<hr>';
        }
    }
}
