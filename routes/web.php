<?php

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/post/{id}/update', 'HomeController@update');
Route::get('/roles-permissions', 'HomeController@rolesPermissions');